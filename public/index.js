const btnWrapper = document.querySelector('.btn-wrapper')
const loader = document.querySelector('.loader')
const btn = document.querySelector('button')
const inputDateStart = document.querySelector('#dateStart')
const inputDateEnd = document.querySelector('#dateEnd')
const linkWrap = document.querySelector('.link-elem')
   
    btn.addEventListener('click', e => {
      loader.style.display = 'flex'
      btnWrapper.style.display  = 'none'
      linkWrap.innerHTML = ''

      const dateStart = inputDateStart.value
      const dateEnd = inputDateEnd.value
      
      fetch(`get_report?startdate=${dateStart}&enddate=${dateEnd}`)
      .then(data => data.text())
      .then(url => {
        loader.style.display = 'none'
        btnWrapper.style.display  = 'flex'
        const a = document.createElement('a')
        a.href = url
        a.innerText = 'Ссылка на отчет'
        linkWrap.append(a)
      })
    })