const { default: axios } = require('axios')
const fs = require('fs')
const conf = require('../conf')

async function uploadFile(fileName) {
  const rootApi = 'https://cloud-api.yandex.net/v1/disk/resources'
  const fileInfo = rootApi + `/?path=/${fileName}`
  const apiUpload = rootApi + `/upload?path=/${fileName}&overwrite=true`
  const apiPublish = rootApi + `/publish?path=/${fileName}`
  const token = `OAuth ${conf.yandex_token}`
  
  const params = {
    headers: {
      'Authorization': token
    }
  }
 
  const response = await axios(apiUpload, params)
  await axios.put(response.data.href, fs.createReadStream('./' + fileName))
  await axios.put(apiPublish, null, params)
  const { data } = await axios(fileInfo, params)
  return data.public_url
}

module.exports = {
  uploadFile
}