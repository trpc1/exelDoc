const { getEstimateTimeWithUsers } = require('./api-bitrix')
const { generateExcel } = require('./generate-excel')
const { uploadFile } = require('./api-yandex')
const conf = require('../conf')

async function generateReport(startdate, enddate) {
  console.log(startdate, enddate)
  const data = await getEstimateTimeWithUsers(startdate, enddate)
  const fileName = await generateExcel(data, conf.file_name)
  const url = await uploadFile(fileName)
  return url
}

module.exports = {
  generateReport
}