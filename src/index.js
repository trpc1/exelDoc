const http = require('http')
const conf = require('../conf')
const { server } = require('./server')

const host = conf.host
const port = conf.port
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`)
})
