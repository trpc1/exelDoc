const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { generateReport } = require('./generateReport')

app.use(express.static("public"))

app.get("/get_report", async (req, res) => {
    const fileLink = await generateReport(req.query.startdate, req.query.enddate)
    res.writeHead(200, { 
      'Content-Type' : 'text/plain' 
    })
    res.end(fileLink)
  
});

module.exports = {
  server
}