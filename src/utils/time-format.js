function timeFormat(seconds) {
  const hours = Math.floor(seconds / 3600)

  seconds %= 3600
  const minutes = Math.floor(seconds / 60)

  seconds %= 60

  const hoursString = hours > 99 ? hours.toString() : `0${hours}`.slice(-2)

  return `${hoursString}:${`0${minutes}`.slice(-2)}`
}

module.exports = {
  timeFormat
}